<?php
require(__DIR__ . '/vendor/autoload.php');


$messaggioIniziale = "Ciao {cognome} {nome}\r\nil tuo pin è: {pin}";

use PhpOffice\PhpSpreadsheet\IOFactory;


$transport = (new Swift_SmtpTransport('10.10.0.1', 25));
$mailer = new Swift_Mailer($transport);
$inputFileName = __DIR__ . '/lista.xlsx';
$spreadsheet = IOFactory::load($inputFileName);
$sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

$fields = $sheetData[1];

foreach ($sheetData as $r => $row) {
    $testoMessaggio = $messaggioIniziale;
    if ($r == 1) {
        continue;
    }
    foreach ($fields as $colonna => $nome) {
        if ($nome == 'email') {
            $email = $row[$colonna];
        }
        $testoMessaggio = str_replace('{' . $nome . '}', $row[$colonna], $testoMessaggio);
    }
    echo "Invio messaggio a $email \r\n";
    $message = (new Swift_Message('Avviso'))
        ->setFrom(['mioIndirizzo@miodominio.it' => 'mio Indirizzo'])
        ->setTo($email)
        ->setBody($testoMessaggio)
    ;
    $result = $mailer->send($message);
}
